﻿#include "chrono/physics/ChSystemNSC.h"
#include "chrono/solver/ChSolverMINRES.h"
#include "chrono/fea/ChElementSpring.h"
#include "chrono/fea/ChElementBar.h"
#include "chrono/fea/ChElementTetra_4.h"
#include "chrono/fea/ChElementTetra_10.h"
#include "chrono/fea/ChElementHexa_20.h"
#include "chrono/fea/ChMesh.h"
#include "chrono/fea/ChMeshFileLoader.h"
#include "chrono/fea/ChLinkPointFrame.h"
#include "chrono/fea/ChVisualizationFEAmesh.h"
#include "chrono_irrlicht/ChIrrApp.h"
#include "chrono/fea/ChElementCableANCF.h"
#include "chrono/physics/ChBodyEasy.h"
#include "chrono/fea/ChLinkDirFrame.h"
#include "chrono/fea/ChLinkPointPoint.h"
#include <memory>
#include <fstream>

using namespace chrono;
using namespace chrono::fea;
using namespace chrono::irrlicht;
using namespace irr;

void exportmesh(std::shared_ptr<ChMesh> mesh, std::string file) {
	std::ofstream os(file);
	//writing node number and nodes;
	os << mesh->GetNnodes() << std::endl;
	for (auto& iter : mesh->GetNodes()) {
		std::shared_ptr<ChNodeFEAxyz> converted;
		converted = std::dynamic_pointer_cast<ChNodeFEAxyz>(iter);
		os << converted->GetIndex() << " " << converted->GetX0()[0] << " " << converted->GetX0()[1] << " " << converted->GetX0()[2] << std::endl;
	}
	//writing number of tetrahedrons and tetrahedrons

	os << mesh->GetNelements() << std::endl;

	int id = 0;
	for (auto& iter : mesh->GetElements()) {
		std::shared_ptr<ChElementTetra_4> converted_tetra;
		converted_tetra = std::dynamic_pointer_cast<ChElementTetra_4>(iter);
		id++;
		os << id << " " << converted_tetra->GetNodeN(0)->GetIndex() << " " << converted_tetra->GetNodeN(1)->GetIndex() << " " << converted_tetra->GetNodeN(2)->GetIndex() << " " << converted_tetra->GetNodeN(3)->GetIndex() << std::endl;
	}

	//writing the strain for each tetrahedron :D
	os << mesh->GetNelements() << std::endl;

	id = 0;
	for (auto& iter : mesh->GetElements()) {
		std::shared_ptr<ChElementTetra_4> converted_tetra;
		converted_tetra = std::dynamic_pointer_cast<ChElementTetra_4>(iter);
		id++;
		os << id << " " << converted_tetra->GetStrain().GetElement(0, 0) + converted_tetra->GetStrain().GetElement(1, 1) + converted_tetra->GetStrain().GetElement(2, 2) << std::endl;
	}

}


int main(int argc, char* argv[]) {

	SetChronoDataPath("data\\");

	ChSystemNSC my_system;

	ChIrrApp application(&my_system, L"Irrlicht FEM visualization", core::dimension2d<u32>(800, 600), false, true);


	application.AddTypicalLogo();
	application.AddTypicalSky();
	application.AddTypicalLights();
	application.AddTypicalCamera(core::vector3df(0, (f32)0.6, -1));

	auto my_mesh = std::make_shared<ChMesh>();

	auto mmaterial = std::make_shared<ChContinuumPlasticVonMises>();
	mmaterial->Set_E(0.01e9);
	mmaterial->Set_v(0.4);
	mmaterial->Set_RayleighDampingK(0.000);
	mmaterial->Set_density(0.7);

	try {
		ChMeshFileLoader::FromTetGenFile(my_mesh, "nodes.node",
			"tetra.ele", mmaterial);
	}
	catch (ChException myerr) {
		GetLog() << myerr.what();
		return 0;
	}

	auto mnodelast = std::dynamic_pointer_cast<ChNodeFEAxyz>(my_mesh->GetNode(my_mesh->GetNnodes() - 1));

	//	mnodelast->SetForce(ChVector<>(0, -25, 0));

	mnodelast->SetMass(500);

	my_system.Add(my_mesh);

	auto truss = std::make_shared<ChBody>();
	truss->SetBodyFixed(true);
	my_system.Add(truss);

	auto mboxfloor = std::make_shared<ChBoxShape>();

	truss->AddAsset(mboxfloor);

	for (unsigned int inode = 0; inode < my_mesh->GetNnodes(); ++inode) {
		if (auto mnode = std::dynamic_pointer_cast<ChNodeFEAxyz>(my_mesh->GetNode(inode))) {
			if (mnode->GetPos().x() < 0.01) {
				auto constraint = std::make_shared<ChLinkPointFrame>();
				constraint->Initialize(mnode, truss);
				my_system.Add(constraint);

				constraint->AddAsset(mboxfloor);
			}
		}
	}

	//------------------------------------------------------------------------------------------------
	//----------------------------------------PROJECT P3----------------------------------------------


	//create a box;
	auto box = std::make_shared<ChBodyEasyBox>(0.1, 0.1, 0.1, 10000);
	auto color = std::make_shared<ChColorAsset>();
	color->SetColor(ChColor(0.6f, 0, 0));
	//set mass of the box
	box->SetMass(5);
	box->AddAsset(color);
	box->SetPos(chrono::Vector(0.6, -0.4, 0));
	my_system.Add(box);
	//create a cable for linking
	//############################

	double beam_L = 0.1;
	double beam_diameter = 0.0025;

	// Create a section, i.e. thickness and material properties
	// for beams. This will be shared among some beams.
	auto msection_cable = std::make_shared<ChBeamSectionCable>();
	msection_cable->SetDiameter(beam_diameter);
	msection_cable->SetYoungModulus(0.01e9);
	msection_cable->SetBeamRaleyghDamping(0.01);

	auto msection_cable2 = std::make_shared<ChBeamSectionCable>();
	msection_cable2->SetDiameter(beam_diameter);
	msection_cable2->SetYoungModulus(0.01e9);
	msection_cable2->SetBeamRaleyghDamping(0.01);

	//the two nodes
	auto node1 = std::dynamic_pointer_cast<ChNodeFEAxyz>(my_mesh->GetNode(85));
	auto node2 = std::dynamic_pointer_cast<ChNodeFEAxyz>(my_mesh->GetNode(84));


	// Create the nodes
	auto hnodeancf1 = std::make_shared<ChNodeFEAxyzD>(node1->GetPos());
	auto hnodeancf2 = std::make_shared<ChNodeFEAxyzD>(ChVector<>(0.6, -0.4, 0));
	auto mesh = std::make_shared<ChMesh>();
	mesh->AddNode(hnodeancf1);
	mesh->AddNode(hnodeancf2);

	my_system.Add(mesh);

	auto hnodeancf3 = std::make_shared<ChNodeFEAxyzD>(node2->GetPos());
	auto hnodeancf4 = std::make_shared<ChNodeFEAxyzD>(ChVector<>(0.6, -0.4, 0));
	auto mesh2 = std::make_shared<ChMesh>();
	mesh2->AddNode(hnodeancf3);
	mesh2->AddNode(hnodeancf4);

	my_system.Add(mesh2);

	// Create the element

	auto belementancf1 = std::make_shared<ChElementCableANCF>();

	belementancf1->SetNodes(hnodeancf1, hnodeancf2);
	belementancf1->SetSection(msection_cable);

	mesh->AddElement(belementancf1);


	auto belementancf2 = std::make_shared<ChElementCableANCF>();

	belementancf2->SetNodes(hnodeancf3, hnodeancf4);
	belementancf2->SetSection(msection_cable2);

	mesh2->AddElement(belementancf2);


	auto constraint_pos3 = std::make_shared<ChLinkPointFrame>();
	constraint_pos3->Initialize(hnodeancf4, box);
	my_system.Add(constraint_pos3);


	auto constraint4 = std::make_shared<ChLinkPointPoint>();
	constraint4->Initialize(node2, hnodeancf3);
	my_system.Add(constraint4);


	auto constraint_pos = std::make_shared<ChLinkPointFrame>();
	constraint_pos->Initialize(hnodeancf2, box);
	my_system.Add(constraint_pos);


	auto constraint2 = std::make_shared<ChLinkPointPoint>();
	constraint2->Initialize(node1, hnodeancf1);
	my_system.Add(constraint2);


	//asset for the cable
	auto mvisualizecable = std::make_shared<ChVisualizationFEAmesh>(*(mesh.get()));
	mvisualizecable->SetFEMdataType(ChVisualizationFEAmesh::E_PLOT_SURFACE);
	mvisualizecable->SetColorscaleMinMax(-0.2, 0.2);
	mvisualizecable->SetShrinkElements(true, 0.85);
	mvisualizecable->SetSmoothFaces(true);
	mesh->AddAsset(mvisualizecable);


	auto mvisualizecable2 = std::make_shared<ChVisualizationFEAmesh>(*(mesh2.get()));
	mvisualizecable2->SetFEMdataType(ChVisualizationFEAmesh::E_PLOT_SURFACE);
	mvisualizecable2->SetColorscaleMinMax(-0.2, 0.2);
	mvisualizecable2->SetShrinkElements(true, 0.85);
	mvisualizecable2->SetSmoothFaces(true);
	mesh2->AddAsset(mvisualizecable2);



	//------------------------------------------------------------------------------------------------


	auto box_1 = std::make_shared<ChBoxShape>();
	box_1->GetBoxGeometry().SetLengths(ChVector<>(0.001, 1, 1));
	truss->AddAsset(box_1);
	auto col_1 = std::make_shared<ChColorAsset>();
	col_1->SetColor(ChColor(0.6f, 0, 0));
	truss->AddAsset(col_1);

	auto mvisualizemesh = std::make_shared<ChVisualizationFEAmesh>(*(my_mesh.get()));
	mvisualizemesh->SetFEMdataType(ChVisualizationFEAmesh::E_PLOT_ELEM_STRAIN_VONMISES);
	mvisualizemesh->SetColorscaleMinMax(-0.2, 0.2);
	mvisualizemesh->SetShrinkElements(true, 0.85);
	mvisualizemesh->SetSmoothFaces(true);
	my_mesh->AddAsset(mvisualizemesh);

	// Asset for the initial position of the beam
	auto mvisualizemeshref = std::make_shared<ChVisualizationFEAmesh>(*(my_mesh.get()));
	mvisualizemeshref->SetFEMdataType(ChVisualizationFEAmesh::E_PLOT_SURFACE);
	mvisualizemeshref->SetWireframe(true);
	mvisualizemeshref->SetDrawInUndeformedReference(true);
	my_mesh->AddAsset(mvisualizemeshref);

	auto mvisualizemeshC = std::make_shared<ChVisualizationFEAmesh>(*(my_mesh.get()));
	mvisualizemeshC->SetFEMglyphType(ChVisualizationFEAmesh::E_GLYPH_NODE_DOT_POS);
	mvisualizemeshC->SetFEMdataType(ChVisualizationFEAmesh::E_PLOT_NONE);
	mvisualizemeshC->SetSymbolsThickness(0.006);
	my_mesh->AddAsset(mvisualizemeshC);

	application.AssetBindAll();

	application.AssetUpdateAll();

	my_system.SetupInitial();

	my_system.SetTimestepperType(chrono::ChTimestepper::Type::EULER_IMPLICIT_LINEARIZED);

	my_system.SetSolverType(ChSolver::Type::MINRES);
	my_system.SetSolverWarmStarting(true);
	my_system.SetMaxItersSolverSpeed(40);
	my_system.SetTolForce(1e-10);

	application.SetTimestep(0.001);



	int i = 0;
	while (application.GetDevice()->run()) {
		i++;
		application.BeginScene();

		application.DrawAll();

		application.DoStep();

		application.EndScene();

		//log the most strained and least strained tetra


		auto firstTetra = std::dynamic_pointer_cast<ChElementTetra_4>(my_mesh->GetElement(1));

		auto firstTetraStrain = firstTetra->GetStrain().GetElement(0, 0) + firstTetra->GetStrain().GetElement(1, 1) + firstTetra->GetStrain().GetElement(2, 2);

		auto most = firstTetraStrain;
		auto least = firstTetraStrain;



		for (auto& tetra : my_mesh->GetElements()) {
			auto tetra4 = std::shared_ptr<ChElementTetra_4>();
			tetra4 = std::dynamic_pointer_cast<ChElementTetra_4>(tetra);
			auto strain = tetra4->GetStrain().GetElement(0, 0) + tetra4->GetStrain().GetElement(1, 1) + tetra4->GetStrain().GetElement(2, 2);

			if (strain > most); {
				most = strain;
			}

			if (strain < least); {
				least = least;
			}
			if (i == 200) {
				break;
			}
		}

		if (i < 200) {
			std::cout << "Most: " << most << std::endl;
			std::cout << "Least: " << least << std::endl;
		}
	}

	exportmesh(my_mesh, "beam.vtk");
	system("pause");

	return 0;
}
