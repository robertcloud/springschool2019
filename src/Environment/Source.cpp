// =============================================================================
// PROJECT CHRONO - http://projectchrono.org
//
// Copyright (c) 2014 projectchrono.org
// All rights reserved.
//
// Use of this source code is governed by a BSD-style license that can be found
// in the LICENSE file at the top level of the distribution and at
// http://projectchrono.org/license-chrono.txt.
//
// =============================================================================
// Authors: Alessandro Tasora
// =============================================================================
//
// FEA using the brick element
//
// =============================================================================

#include "chrono/physics/ChSystemNSC.h"
#include "chrono/solver/ChSolverMINRES.h"
#include "chrono/fea/ChElementSpring.h"
#include "chrono/fea/ChElementBrick.h"
#include "chrono/fea/ChElementBar.h"
#include "chrono/fea/ChLinkPointFrame.h"
#include "chrono/fea/ChLinkDirFrame.h"
#include "chrono/fea/ChVisualizationFEAmesh.h"
#include "chrono_irrlicht/ChIrrApp.h"
#include "chrono/fea/ChBuilderBeam.h"
#include "chrono/fea/ChBeamSection.h"
#include "../siswSpringSchool/chrono/src/chrono/fea/ChElementTetra_4.h"

using namespace chrono;
using namespace chrono::fea;
using namespace chrono::irrlicht;

using namespace irr;

int main(int argc, char* argv[]) {

	

	ChSystemNSC my_system;

	// Create the Irrlicht visualization (open the Irrlicht device,
	// bind a simple user interface, etc. etc.)
	ChIrrApp application(&my_system, L"Second handout", core::dimension2d<u32>(800, 600), false, true);

	SetChronoDataPath("E:\\siswSpringSchool\\chrono\\data\\");

	//	my_system.Set_G_acc(ChVector<>(0, 0, 0));

	// Easy shortcuts to add camera, lights, logo and sky in Irrlicht scene:
	application.AddTypicalLogo();
	application.AddTypicalSky();
	application.AddTypicalLights();


	application.AddTypicalCamera(core::vector3df(0, 0, 6),   // camera location
		core::vector3df(0.0f, 0.0f, 0.0f));  // "look at" location

	// Create a body suspended through a ChLinkSpring
	// ----------------------------------------------

	auto body_1 = std::make_shared<ChBody>();
	my_system.AddBody(body_1);
	body_1->SetPos(ChVector<>(-1, -3, 0));
	body_1->SetIdentifier(1);
	body_1->SetBodyFixed(true);
	body_1->SetCollide(false);
	body_1->SetMass(1);
	body_1->SetInertiaXX(ChVector<>(1, 1, 1));

	// Attach a visualization asset.
	auto box_1 = std::make_shared<ChBoxShape>();
	box_1->GetBoxGeometry().SetLengths(ChVector<>(10, 10, 1));
	body_1->AddAsset(box_1);
	auto col_1 = std::make_shared<ChColorAsset>();
	col_1->SetColor(ChColor(0.6f, 0, 0));
	body_1->AddAsset(col_1);

	//------------------------------------------------------------------------------------------------
	
	// Create a mesh, that is a container for groups
	// of elements and their referenced nodes.
	auto my_mesh = std::make_shared<ChMesh>();
	// Remember to add the mesh to the system!
	my_system.Add(my_mesh);
	// Create some point-like nodes with x,y,z degrees of freedom
	// While creating them, also set X0 undeformed positions.
	auto mnode1 = std::make_shared<ChNodeFEAxyz>(ChVector<>(0, 0, 0));
	auto mnode2 = std::make_shared<ChNodeFEAxyz>(ChVector<>(0, 0, 1));
	auto mnode3 = std::make_shared<ChNodeFEAxyz>(ChVector<>(0, 1, 0));
	auto mnode4 = std::make_shared<ChNodeFEAxyz>(ChVector<>(1, 0, 0));
	// Remember to add nodes and elements to the mesh!
	my_mesh->AddNode(mnode1);
	my_mesh->AddNode(mnode2);
	my_mesh->AddNode(mnode3);
	my_mesh->AddNode(mnode4);

	// For example, set some non-zero mass concentrated at the nodes
	mnode1->SetMass(0.01);
	mnode2->SetMass(0.01);
	// For example, set an applied force to a node:
	mnode2->SetForce(ChVector<>(0, 5, 0));

	// Create a material, that must be assigned to each element,
	auto mmaterial = std::make_shared<ChContinuumElastic>();
	// �and set its parameters
	mmaterial->Set_E(0.01e9); // rubber 0.01e9, steel 200e9
	mmaterial->Set_v(0.3);

	// Create the tetrahedron element,
	auto melement1 = std::make_shared<ChElementTetra_4>();
	// Remember to add elements to the mesh!
	my_mesh->AddElement(melement1);
	// assign nodes
	melement1->SetNodes(mnode1, mnode2, mnode3, mnode4);
	// assign material
	melement1->SetMaterial(mmaterial);

	
	// Analysis � Example 1: dynamics
	double timestep = 0.01;
	while (my_system.GetChTime() < 2) {
		my_system.DoStepDynamics(timestep);
	}

	auto mvisualizemesh = std::make_shared<ChVisualizationFEAmesh>(*(my_mesh.get()));
	mvisualizemesh->SetFEMdataType(ChVisualizationFEAmesh::E_PLOT_NODE_SPEED_NORM);
	mvisualizemesh->SetColorscaleMinMax(0.0, 5.50);
	mvisualizemesh->SetShrinkElements(true, 0.85);
	mvisualizemesh->SetSmoothFaces(true);
	my_mesh->AddAsset(mvisualizemesh);

	//------------------------------------------------------------------------------------------------

	application.AssetBindAll();
	application.AssetUpdateAll();

	
	my_system.SetupInitial();

	// Mark completion of system construction
	my_system.SetupInitial();
	// The FEA problems require MINRES solvers! (or MKL or MUMPS optional solvers)
	my_system.SetSolverType(ChSolver::Type::MINRES);
	// Analysis � Example 1: a static analysis
	my_system.DoStaticLinear();

	application.SetTimestep(0.004);

	while (application.GetDevice()->run()) {
		application.BeginScene();
		application.DrawAll();
		application.DoStep();
		application.EndScene();
	}

	return 0;
}
