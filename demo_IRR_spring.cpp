// =============================================================================
// PROJECT CHRONO - http://projectchrono.org
//
// Copyright (c) 2014 projectchrono.org
// All rights reserved.
//
// Use of this source code is governed by a BSD-style license that can be found
// in the LICENSE file at the top level of the distribution and at
// http://projectchrono.org/license-chrono.txt.
//
// =============================================================================
// Authors: Radu Serban
// =============================================================================
//
// Simple example demonstrating the use of ChLinkSpring and ChLinkSpringCB.
//
// Two bodies, connected with identical (but modeled differently) spring-dampers
// are created side by side.
//
// Recall that Irrlicht uses a left-hand frame, so everything is rendered with
// left and right flipped.
//
// =============================================================================


#include "chrono/physics/ChSystemNSC.h"
#include "chrono/fea/ChVisualizationFEAmesh.h"
#include "chrono_irrlicht/ChIrrApp.h"
#include "chrono/fea/ChElementTetra_4.h"

using namespace chrono;
using namespace chrono::irrlicht;
using namespace chrono::fea;
using namespace irr;


// =============================================================================

int main(int argc, char* argv[]) {
    GetLog() << "Copyright (c) 2017 projectchrono.org\nChrono version: " << CHRONO_VERSION << "\n\n";

    ChSystemNSC system;
    //system.Set_G_acc(ChVector<>(0, 0, 0));

	SetChronoDataPath("C:\\Users\\John Doe\\Documents\\SmartGit\\siemensspringschool\\Chrono\\data\\");

    // Create a body suspended through a ChLinkSpring
    // ----------------------------------------------

    auto body_1 = std::make_shared<ChBody>();
    system.AddBody(body_1);
    body_1->SetPos(ChVector<>(-1, -3, 0));
    body_1->SetIdentifier(1);
    body_1->SetBodyFixed(true);
    body_1->SetCollide(true);
    body_1->SetMass(1);
    body_1->SetInertiaXX(ChVector<>(1, 1, 1));

    // Attach a visualization asset.
    auto box_1 = std::make_shared<ChBoxShape>();
    box_1->GetBoxGeometry().SetLengths(ChVector<>(10, 10, 1));
    body_1->AddAsset(box_1);
    auto col_1 = std::make_shared<ChColorAsset>();
    col_1->SetColor(ChColor(0.6f, 0, 0));
    body_1->AddAsset(col_1);


	// Create a mash
    // ------------------------------------------------
	auto myMesh = std::make_shared<ChMesh>();

	//Barna
    //auto l_vis = std::make_shared<ChBoxShape>();
    //l_vis->GetBoxGeometry().SetLengths(ChVector<>(1, 1, 10));
    //myMesh->AddAsset(l_vis);

	auto mnode1 = std::make_shared<ChNodeFEAxyz>(ChVector<>(0, 0, 0));
    auto mnode2 = std::make_shared<ChNodeFEAxyz>(ChVector<>(0, 0, 1));
    auto mnode3 = std::make_shared<ChNodeFEAxyz>(ChVector<>(0, 1, 0));
    auto mnode4 = std::make_shared<ChNodeFEAxyz>(ChVector<>(1, 0, 0));

    myMesh->AddNode(mnode1);
    myMesh->AddNode(mnode2);
    myMesh->AddNode(mnode3);
    myMesh->AddNode(mnode4);

	myMesh->GetNodes().front()->SetFixed(true);

    // Set some non-zero mass concentrated at the nodes
	mnode1->SetMass(0.01);
    mnode2->SetMass(0.01); 
	// Set an applied force to a node:
	mnode2->SetForce(ChVector<>(0, 5, 0));

	// Create a material, that will be assigned to each element,
    auto mmaterial = std::make_shared<ChContinuumElastic>();
    // Set its parameters
    mmaterial->Set_E(0.01e9);  // rubber 0.01e9, steel 200e9
    mmaterial->Set_v(0.3);

	// Create the tetrahedron element,

    auto l_vis = std::make_shared<ChBoxShape>();
    l_vis->GetBoxGeometry().SetLengths(ChVector<>(1, 1, 10));
    myMesh->AddAsset(l_vis);

    auto melement1 = std::make_shared<ChElementTetra_4>();
    // Add elements to the mesh!
    myMesh->AddElement(melement1);
    // Assign nodes
    melement1->SetNodes(mnode1, mnode2, mnode3, mnode4);
    // Assign material
    melement1->SetMaterial(mmaterial);

	system.Add(myMesh);

	// Mark completion of system construction
    system.SetupInitial();
    // The FEA problems require MINRES solvers! (or MKL or MUMPS optional solvers)
    system.SetSolverType(ChSolver::Type::MINRES);

	//I don't need this
    //// Analysis � Example 1: a static analysis
    //system.DoStaticLinear();
    //// Analysis � Example 1: dynamics
    //double timestep = 0.01;
    //while (system.GetChTime() < 2) {
    //    system.DoStepDynamics(timestep);
    //}


	auto mvisualizemesh = std::make_shared<ChVisualizationFEAmesh>(*(myMesh.get()));
    mvisualizemesh->SetFEMdataType(ChVisualizationFEAmesh::E_PLOT_NODE_SPEED_NORM);
    mvisualizemesh->SetColorscaleMinMax(0.0, 5.50);
    mvisualizemesh->SetShrinkElements(true, 0.85);
    mvisualizemesh->SetSmoothFaces(true);
    myMesh->AddAsset(mvisualizemesh);



    // Create a body suspended through a ChLinkSpringCB
    // ------------------------------------------------

    //auto body_2 = std::make_shared<ChBody>();
    //system.AddBody(body_2);
    //body_2->SetPos(ChVector<>(1, 10, 0));
    //body_2->SetIdentifier(1);
    //body_2->SetBodyFixed(false);
    //body_2->SetCollide(true);
    //body_2->SetMass(10);
    //body_2->SetInertiaXX(ChVector<>(1, 1, 1));

    //// Attach a visualization asset.
    //auto box_2 = std::make_shared<ChBoxShape>();
    //box_2->GetBoxGeometry().SetLengths(ChVector<>(1, 1, 7));
    //body_2->AddAsset(box_2);
    //auto col_2 = std::make_shared<ChColorAsset>();
    //col_2->SetColor(ChColor(0, 0, 0.6f));
    //body_2->AddAsset(col_2);



    // Create the Irrlicht application
    // -------------------------------

    ChIrrApp application(&system, L"ChBodyAuxRef demo", core::dimension2d<u32>(800, 600), false, true);
    application.AddTypicalLogo();
    application.AddTypicalSky();
    application.AddTypicalLights();
    application.AddTypicalCamera(core::vector3df(0, 0, 20));

    application.AssetBindAll();
    application.AssetUpdateAll();

    // Simulation loop
    int frame = 0;

    application.SetTimestep(0.001);

    GetLog().SetNumFormat("%10.3f");

    while (application.GetDevice()->run()) {
        application.BeginScene();

        application.DrawAll();

		application.DoStep();

        application.EndScene();
    }

    return 0;
}